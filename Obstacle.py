class Obstacle(object):
    """Two types of obstacles, one from up and one from the bottom."""
    top_obstacle = pygame.image.load("top_obstacle.png")
    bottom_obstacle = pygame.image.load("bottom_obstacle.png")

    """A class for pairs of obstacles in the game that are moving from right to left."""
    def __init__(self):
        """Creating a pair of obstacles outside of the screen."""
        self.x = width
        self.y = random.randint(0, 345)
        self.gap = random.randint(obstacle_gap_range[0], obstacle_gap_range[1])
        self.speed = obstacle_speed
        self.cross = False

    def update(self):
        """Moving the pair of obstacles to the screen."""
        self.x -= self.speed

    def draw_obstacle(self):
        """Drawing the pair of the obstacles on the screen."""
        screen.blit(self.top_obstacle, (self.x, self.y - 495))
        screen.blit(self.bottom_obstacle, (self.x, self.y + self.gap))