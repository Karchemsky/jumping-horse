__author__ = 'George Karchemsky'

import pygame
import random
import time
import winsound

game_background = pygame.image.load("game_background.png")
game_over_menu = pygame.image.load("game_over_menu.png")
start_menu = pygame.image.load("start_menu.png")
pygame.init()
width = 495
height = 495
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Ride The Horse")
screen.blit(start_menu, (0, 0))
EXIT = False
obstacle_per_frames = 700
obstacle_speed = 0.5
obstacle_gap_range = [180, 220]
scores = []
frames = 0
points = 0
playing_game = False
press_to_start = False


def start_play():
    winsound.PlaySound('game_song.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
    start_message = pygame.font.SysFont(None, 95).render(str("Ride The Horse"), 1, (0, 0, 0))
    press_message = pygame.font.SysFont(None, 30).render(str("Press on the horse"), 1, (0, 0, 0))
    screen.blit(game_background, (0, 0))
    screen.blit(press_message, (47,  height / 2 - 30))
    screen.blit(start_message, (0, 20))
    pygame.display.update()


def game_over(list_obstacles, playing_horse):
    pygame.mouse.set_visible(True)
    playing_horse.die()
    for every_obstacle_to_stop in list_obstacles:
        every_obstacle_to_stop.speed = 0.0
    winsound.PlaySound('end.wav', winsound.SND_ALIAS | winsound.SND_ASYNC)
    time.sleep(0.5)
    screen.blit(game_over_menu, (0, 0))


class Obstacle(object):
    top_obstacle = pygame.image.load("top_obstacle.png")
    bottom_obstacle = pygame.image.load("bottom_obstacle.png")

    def __init__(self):
        self.x = width
        self.y = random.randint(0, 345)
        self.gap = random.randint(obstacle_gap_range[0], obstacle_gap_range[1])
        self.speed = obstacle_speed
        self.cross = False

    def update(self):
        self.x -= self.speed

    def draw_obstacle(self):
        screen.blit(self.top_obstacle, (self.x, self.y - 495))
        screen.blit(self.bottom_obstacle, (self.x, self.y + self.gap))


class Horse(object):
    horse_regular = pygame.image.load("horse_regular.png")
    horse_up_one = pygame.image.load("horse_up_one.png")
    horse_up_two = pygame.image.load("horse_up_two.png")
    horse_down_one = pygame.image.load("horse_down_one.png")
    horse_down_two = pygame.image.load("horse_down_two.png")
    horse_middle = pygame.image.load("horse_middle.png")

    def __init__(self):
        self.x = 100
        self.y = height / 2
        self.gravity = 0.02
        self.velocity = 0
        self.image = self.horse_regular

    def jump(self):
        self.velocity -= 1.4
        if self.velocity < -2:
            self.velocity = -2
        self.gravity = 0.02

    def update(self):
        self.velocity += self.gravity
        self.y += self.velocity
        if self.velocity > 1.5:
            self.velocity = 1.5

    def draw_horse(self):
        if self.velocity > 0:
            if self.velocity > 1:
                self.image = self.horse_down_two

            else:
                self.image = self.horse_down_one

        elif self.velocity < 0:
            if self.velocity < -1:
                self.image = self.horse_up_two

            else:
                self.image = self.horse_up_one

        else:
            self.image = self.horse_middle

        if self.y > height - 25 or self.y < 0:
            self.die()

        screen.blit(self.image, (self.x, self.y))

    def die(self):
        self.gravity = 0
        self.velocity = 0


horse = Horse()  # Creating the horse
obstacle = Obstacle()  # Creating a pair of obstacles
obstacles = list()  # A list that will contain all the obstacles
obstacles.append(obstacle)
horse.draw_horse()
show_points = pygame.font.SysFont(None, 70).render(str(points), 1, (255, 255, 255))
pygame.display.update()



while not EXIT:
    pygame.display.update()
    for event in pygame.event.get():
            if event.type == pygame.QUIT:
                EXIT = True
            elif pygame.mouse.get_pressed()[0] == 1:
                if not press_to_start:
                    if pygame.mouse.get_pos()[0] in range(88, 177) and pygame.mouse.get_pos()[1] in range(435, 474):
                        EXIT = True
                    if pygame.mouse.get_pos()[0] in range(254, 425) and pygame.mouse.get_pos()[1] in range(435, 473):
                        press_to_start = True
                        horse = Horse()
                        obstacle = Obstacle()
                        obstacles = list()
                        obstacles.append(obstacle)
                        horse.draw_horse()
                        show_points = pygame.font.SysFont(None, 70).render(str(points), 1, (255, 255, 255))
                        pygame.display.update()
                else:
                    if pygame.mouse.get_pos()[0] in range(50, 150) and pygame.mouse.get_pos()[1] in range(197, 297):
                        playing_game = True
    while press_to_start:
        start_play()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                EXIT = True
        while playing_game:
            pass


pygame.quit()